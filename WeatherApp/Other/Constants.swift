//
//  Constants.swift
//  WeatherApp
//
//  Created by Mac on 29.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

struct EndPoints {
    static let baseURL = "http://api.openweathermap.org/data/2.5"
    static let currentWeatherURLPart = "/weather?"
    static let forecastWeatherURLPart = "/forecast/daily?"
}

struct GooglePlacesAPI {
    static let key = "AIzaSyCmTI9w0zwL5On0P2q_sgRqct10vsT1N3g"
}
