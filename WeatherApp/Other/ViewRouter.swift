//
//  ViewRouter.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

protocol ViewRouter {
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

extension ViewRouter {
    func prepare(for segue: UIStoryboardSegue, sender: Any?) { }
}
