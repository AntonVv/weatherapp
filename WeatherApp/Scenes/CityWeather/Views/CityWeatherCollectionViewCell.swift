//
//  CityWeatherCollectionViewCell.swift
//  WeatherApp
//
//  Created by Mac on 30.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

class CityWeatherCollectionViewCell: UICollectionViewCell, CityWeatherCellView {

    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var dayTempLabel: UILabel!
    @IBOutlet private weak var nightTempLabel: UILabel!
    @IBOutlet private weak var backImageView: UIImageView!
    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func display(weather: Weather?) {
        
        if let weather = weather {
            self.dateLabel.text = weather.dateString
            self.dayTempLabel.text = weather.dayTempCelsiusString
            self.nightTempLabel.text = weather.nightTempCelsiusString
            self.iconImageView.image = UIImage(named: weather.iconName)
            self.backImageView.image = UIImage(named: weather.iconName)
            self.backImageView.alpha = 0.1
            self.infoLabel.text = weather.fullInfoDescription
        }
    }
    
    override var isSelected: Bool {
        didSet {
            self.contentView.backgroundColor = isSelected ?  UIColor(white:1.0, alpha:0.1) : UIColor.clear
        }
    }
}

extension CityWeatherCollectionViewCell {
    static func cellReuseIdentifier() -> String {
        return String(describing: self)
    }
}

