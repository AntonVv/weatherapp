//
//  ForecastChartView.swift
//  WeatherApp
//
//  Created by Mac on 30.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Charts

class ForecastChartView: LineChartView  {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        config()
    }
    
    func display(for forecast: [Weather]?) {
        
        guard let forecast = forecast else {
            return
        }
        
        var lineEntryArray = [ChartDataEntry]()
        
        for (index, weather) in forecast.enumerated() {
            
            let lineEntry = BarChartDataEntry(x: Double(index), y: Double(weather.midTemp!), data: nil)
            lineEntryArray.append(lineEntry)
        }
        
        let lineDataSet = LineChartDataSet(values: lineEntryArray, label: "Line chart data")
        lineDataSet.mode = .linear
        lineDataSet.setColor(UIColor.white)
        lineDataSet.drawCirclesEnabled = false
        lineDataSet.drawValuesEnabled = false
        lineDataSet.highlightColor = UIColor.white
        lineDataSet.highlightLineWidth = 1.0
        lineDataSet.drawHorizontalHighlightIndicatorEnabled = false
        let gradientColors = [ChartColorTemplates.colorFromString("#394359").cgColor,
                              ChartColorTemplates.colorFromString("#FFFFFF").cgColor]
        let gradinet = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)
        lineDataSet.fillAlpha = 0.4
        lineDataSet.fill = Fill.fillWithLinearGradient(gradinet!, angle: 90.0)
        lineDataSet.drawFilledEnabled = true
        let lineData = LineChartData(dataSet: lineDataSet)
        self.data = lineData
        self.fitScreen()
    }
    
    func config() {
        self.scaleYEnabled = false
        self.scaleXEnabled = false
        self.noDataText = "Нет данных для отображения"
        self.chartDescription?.text = ""
        self.minOffset = 0.0
        self.extraTopOffset = 8.0
        self.extraBottomOffset = 8.0
        self.xAxis.enabled = false
        self.legend.enabled = false
        self.leftAxis.gridColor = .clear
        self.leftAxis.axisLineColor = .clear
        self.rightAxis.axisLineColor = .clear
        self.leftAxis.labelTextColor = .white
        self.rightAxis.labelTextColor = .white
    }
}

