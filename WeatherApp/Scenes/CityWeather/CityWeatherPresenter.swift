//
//  CityWeatherPresenter.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit
import Charts

protocol CityWeatherView: class {
    var collectionView: UICollectionView! {get}
    
    func reloadView()
    func selectItem(at index:Int)
    func displayInfo(for city:City)
    func hideActivityIndicator()
    func showActivityIndicator()
    func itemSize(_ collectionView: UICollectionView) -> CGSize
}

protocol CityWeatherCellView {
    func display(weather: Weather?)
}

protocol CityWeatherChartView: class {
    
    var chartView: ForecastChartView! { get }
}

protocol CityWeatherPresenter {
    
    func setActiveForecastPeriod(period: Int)
    var numberOfForecastWeathers: Int { get }
    var router: CityWeatherViewRouter { get }
    var city: City {get}
    func refreshForecastAndCurrent()

    func viewDidLoad()
    func configure(cell: CityWeatherCellView, forRow row: Int)
    func didSelect(row: Int)
    
}

final class CityWeatherPresenterImplementation: CityWeatherPresenter {

    var city: City
    var forecastPeriod = WeatherPeriod.forecast5Days
    
    fileprivate weak var view: CityWeatherView?
    fileprivate weak var chartView: CityWeatherChartView?

    fileprivate let getWeatherUseCase: GetWeatherUseCase

    let router: CityWeatherViewRouter

    var numberOfForecastWeathers: Int {
        
        if self.city.weatherForecast!.count >= forecastPeriod.weathersCount {
            return forecastPeriod.weathersCount
        }
        return self.city.weatherForecast!.count
    }
    
    init(view: CityWeatherView,
         router: CityWeatherViewRouter, city: City, getWeatherUseCase: GetWeatherUseCase, chartView: CityWeatherChartView) {
        self.view = view
        self.chartView = chartView
        self.router = router
        self.city = city
        self.getWeatherUseCase = getWeatherUseCase
    }
    
    
    // MARK: - CityWeatherPresenter

    func viewDidLoad() {
        self.view?.collectionView.registerCellClassesWithNibs(CityWeatherCollectionViewCell.self)
        guard let flowLayout = self.view?.collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.itemSize = (self.view?.itemSize((self.view?.collectionView)!))!
        self.displayForecast(for: city)
        self.view?.displayInfo(for: city)
        
        if let forecast = self.city.weatherForecast {
            self.chartView?.chartView.display(for: Array(forecast.prefix(forecastPeriod.weathersCount)))
        }
        
        self.chartView?.chartView.delegate = self
        
        if city.didUpdateForecastonThisLaunch == false {
            self.refreshForecast()
        }
    }
    
    func configure(cell: CityWeatherCellView, forRow row: Int) {
        let weather = self.city.weatherForecast?[row]
        cell.display(weather: weather)
    }
    
    func didSelect(row: Int) {
        
    }
    
    func setActiveForecastPeriod(period: Int) {
        forecastPeriod = WeatherPeriod(rawValue: period)!
        self.handleWeatherPeriodDidChange()
    }
    
    func refreshForecastAndCurrent() {
        
        refreshForecast()
        refeshCurrentWeather()
    }
    
    func refreshForecast() {
        view?.showActivityIndicator()
        getWeatherUseCase.getWeather(in: city, period: .forecast16Days) { (result) in
            switch result {
            case let .success(resultCity):
                self.handleGetWheather(resultCity)
            case let .failure(error):
                self.handleGetWheatherError(error)
            }
        }
    }
    
    func refeshCurrentWeather() {
        getWeatherUseCase.getCurrentWeather(in: city) { (result) in
            switch result {
            case let .success(resultCity):
                self.handleGetWheather(resultCity)
            case let .failure(error):
                self.handleGetWheatherError(error)
            }
        }
    }
    
    // MARK: - private
    
    func displayForecast(for city:City) {
        
    }
    
    func handleWeatherPeriodDidChange() {
        if let forecast = self.city.weatherForecast {
            self.chartView?.chartView.display(for: Array(forecast.prefix(forecastPeriod.weathersCount)))
        }
        view?.reloadView()
    }
    
    fileprivate func handleGetWheather(_ city:City) {
        self.city = city
        self.city.didUpdateForecastonThisLaunch = true
        self.view?.displayInfo(for: city)
        handleWeatherPeriodDidChange()
        NotificationCenter.default.post(name: Notification.Name("updateCity"), object: city)
        view?.hideActivityIndicator()
    }
    
    fileprivate func handleGetWheatherError(_ error:Error) {
        view?.hideActivityIndicator()
    }
}

extension CityWeatherPresenterImplementation: ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        self.view?.collectionView.selectItem(at: IndexPath(row:Int(entry.x), section:0), animated: true, scrollPosition: .centeredHorizontally)
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        if let selectedPathes = self.view?.collectionView.indexPathsForSelectedItems {
            self.view?.collectionView.deselectItem(at:selectedPathes.last!, animated: true)
        }
    }
}

extension UICollectionView {
    
    public func registerCellClassesWithNibs(_ classes: AnyClass...)
    {
        for cellClass in classes
        {
            self.register(UINib(nibName: String(describing: cellClass), bundle: nil), forCellWithReuseIdentifier: String(describing: cellClass))
        }
    }
}

