//
//  CityWeatherViewController.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

class CityWeatherViewController: UIViewController, CityWeatherView,CityWeatherChartView {


    var configurator = CityWeatherConfiguratorImplementation()
    var presenter: CityWeatherPresenter?
    
    @IBOutlet internal weak var chartView: ForecastChartView!
    @IBOutlet private weak var refreshButton: UIBarButtonItem!
    @IBOutlet private weak var conditionNameLabel: UILabel!
    @IBOutlet private weak var cityNameLabel: UILabel!
    @IBOutlet private weak var currentTempLabel: UILabel!
    @IBOutlet internal weak var collectionView: UICollectionView!
    var activityIndicator: UIActivityIndicatorView?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

    
    // MARK: - IBAction

    @IBAction func segmentedControlAction(_ sender: UISegmentedControl) {
        presenter?.setActiveForecastPeriod(period: sender.selectedSegmentIndex)
    }
    
    @IBAction func refreshButtonAction(_ sender: Any) {
        presenter?.refreshForecastAndCurrent()
    }
    
    // MARK: - CityWeatherView

    func reloadView() {
        self.collectionView.reloadData()
    }
    
    func showActivityIndicator() {
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        let barItem = UIBarButtonItem(customView: self.activityIndicator!)
        self.navigationItem.rightBarButtonItem = barItem
        self.activityIndicator?.startAnimating()
    }
    
    func hideActivityIndicator() {
        self.activityIndicator?.stopAnimating()
        let barItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshButtonAction(_:)))
        self.navigationItem.rightBarButtonItem = barItem
    }
    
    func displayInfo(for city: City) {
        self.cityNameLabel.text = city.name
        self.currentTempLabel.text = city.currentWeather?.midTempCelsiusString
        self.conditionNameLabel.text = city.currentWeather?.weatherConditionName
    }
    
    func selectItem(at index: Int) {
        
    }
}

extension CityWeatherViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CityWeatherCollectionViewCell.cellReuseIdentifier(), for: indexPath) as! CityWeatherCollectionViewCell
        presenter?.configure(cell: cell, forRow: indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let presenter = self.presenter else {
            return 0
        }
        return presenter.numberOfForecastWeathers
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func viewDidLayoutSubviews() {
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.itemSize = itemSize(collectionView)
        flowLayout.invalidateLayout()
    }
    
    func itemSize(_ collectionView: UICollectionView) -> CGSize {
        
        let minLength = fmin(collectionView.frame.size.width,collectionView.frame.size.height)
        let maxLength = fmax(collectionView.frame.size.width,collectionView.frame.size.height)
        return CGSize(width: maxLength/3, height: minLength)
    }

}
