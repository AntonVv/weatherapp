//
//  CityWeatherConfigurator.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

protocol CityWeatherConfigurator {
    func configure(citiesViewController: CityWeatherViewController, city: City)
}

class CityWeatherConfiguratorImplementation: CityWeatherConfigurator {
    
    func configure(citiesViewController: CityWeatherViewController, city: City) {
        
        let viewContext = CoreDataStackImplementation.sharedInstance.persistentContainer.newBackgroundContext()
        
        let apiWeatherService = ApiWeatherServiceImplementation(urlSessionConfiguration: URLSessionConfiguration.default,
                                                                completionHandlerQueue: OperationQueue.main)
        let apiWeatherGateway = ApiWeatherSearchGatewayImplementation(apiWeatherService: apiWeatherService)
        
        let coreDataWeatherGateway = CoreDataWeatherGateway(viewContext: viewContext)
        
        let getWeatherUseCase = GetWeatherUseCaseImplementation(apiWeatherSearchGateway: apiWeatherGateway, coreDataWeatherGateway: coreDataWeatherGateway)
        
        let cityWeatherRouter = CityWeatherViewRouterImplementation()
        
        let cityWeatherPresenter = CityWeatherPresenterImplementation(view: citiesViewController, router: cityWeatherRouter, city: city, getWeatherUseCase: getWeatherUseCase, chartView: citiesViewController)
        citiesViewController.presenter = cityWeatherPresenter
    }    
}
