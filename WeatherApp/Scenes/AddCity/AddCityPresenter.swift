//
//  AddCityPresenter.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

protocol AddCityView: class {
    
}

protocol AddCityPresenterDelegate: class {
    func addCityPresenter(_ presenter: AddCityPresenter, didAdd city: City)
    func addCityPresenterCancel(presenter: AddCityPresenter)
}

protocol AddCityPresenter {
    var router: AddCityViewRouter { get }
}

//class AddCityPresenterImplementation: AddCityPresenter {
//    
//}
