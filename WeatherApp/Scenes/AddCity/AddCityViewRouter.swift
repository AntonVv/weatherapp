//
//  AddCityViewRouter.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

protocol AddCityViewRouter {
    func dismiss()
}

class AddCityViewRouterImplementation: AddCityViewRouter {
    fileprivate weak var addCityViewController: AddCityViewController?
    
    init(addCityViewController: AddCityViewController) {
        self.addCityViewController = addCityViewController
    }
    
    func dismiss() {
        self.addCityViewController?.dismiss(animated: true, completion: nil)
    }
}
