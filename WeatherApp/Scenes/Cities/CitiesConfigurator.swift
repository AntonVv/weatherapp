//
//  CitiesConfigurator.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

protocol CitiesConfigurator {
    func configure(citiesViewController: CitiesViewController)
}

class CitiesConfiguratorImplementation: CitiesConfigurator  {
    
    func configure(citiesViewController: CitiesViewController) {
                
        let viewContext = CoreDataStackImplementation.sharedInstance.persistentContainer.newBackgroundContext()
        let localCitiesGateway = CoreDataCitiesGateway(viewContext: viewContext)
        
        let displayCitiesListUseCase = DisplayCitiesListUseCaseImplementation(citiesGateway: localCitiesGateway)
        let addCityUseCase = AddCityUseCaseImplementation(citiesGateway:localCitiesGateway)
        let deleteCityUseCase = DeleteCityUseCaseImplementation(citiesGateway: localCitiesGateway)
        
        
        let apiWeatherService = ApiWeatherServiceImplementation(urlSessionConfiguration: URLSessionConfiguration.default,
                                                                completionHandlerQueue: OperationQueue.main)
        let apiWeatherGateway = ApiWeatherSearchGatewayImplementation(apiWeatherService: apiWeatherService)
        
        let coreDataWeatherGateway = CoreDataWeatherGateway(viewContext: viewContext)
        let getWeatherUseCase = GetWeatherUseCaseImplementation(apiWeatherSearchGateway: apiWeatherGateway, coreDataWeatherGateway: coreDataWeatherGateway)
        
        let router = CitiesViewRouterImplementation(citiesViewController: citiesViewController)
        
        let presenter = CitiesPresenterImplementation(view: citiesViewController,
                                                      displayCitiesUseCase: displayCitiesListUseCase,
                                                      deleteCityUseCase: deleteCityUseCase,
                                                      addCityUseCase: addCityUseCase,
                                                      getWeatherUseCase: getWeatherUseCase,
                                                      router: router)
        
        citiesViewController.presenter = presenter
    }


}
