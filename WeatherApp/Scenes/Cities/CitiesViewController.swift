//
//  CitiesViewController.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController, CitiesView {

    var configurator = CitiesConfiguratorImplementation()
    var presenter: CitiesPresenter?
    
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurator.configure(citiesViewController: self)
        presenter?.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter?.router.prepare(for: segue, sender: sender)
    }
    
    // MARK: - IBAction
    
    @IBAction func addButtonPressed(_ sender: Any) {
        presenter?.addButtonPressed()
    }
    
    // MARK: - CitiesView
    
    func refreshCitiesView() {
        self.tableView.reloadData()
    }
    
    func deleteAnimated(row: Int) {
        tableView.deleteRows(at: [IndexPath(row: row, section:0)], with: .automatic)
    }
    
    func endEditing() {
        tableView.setEditing(false, animated: true)
    }
    
    func refreshCity(at row:Int) {
        self.tableView.reloadRows(at: [IndexPath(row: row, section:0)], with: .fade)
    }
    
    func addAnimated(row: Int) {
        tableView.beginUpdates()
        let indexPath:IndexPath = IndexPath(row:row, section:0)
        tableView.insertRows(at: [indexPath], with: .left)
        tableView.endUpdates()
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
}

extension CitiesViewController: UITableViewDataSource,UITableViewDelegate {
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.numberOfCities
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CityTableViewCell.cellReuseIdentifier(), for: indexPath) as! CityTableViewCell
        presenter?.configure(cell: cell, forRow: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.didSelect(row: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return presenter!.canEdit(row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCellEditingStyle,
                   forRowAt indexPath: IndexPath) {
        presenter?.deleteButtonPressed(row: indexPath.row)
    }
}
