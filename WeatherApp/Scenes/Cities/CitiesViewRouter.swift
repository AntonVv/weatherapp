//
//  CitiesViewRouter.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit
import GooglePlaces

protocol CitiesViewRouter: ViewRouter {
    func presentCityWeather(for city: City)
    func presentAddCity(addCityPresenterDelegate: GMSAutocompleteViewControllerDelegate)
}

final class CitiesViewRouterImplementation: CitiesViewRouter {
    fileprivate weak var citiesViewController: CitiesViewController?
    fileprivate weak var addCityPresenterDelegate: GMSAutocompleteViewControllerDelegate?

    fileprivate var city: City!
    
    init(citiesViewController: CitiesViewController) {
        self.citiesViewController = citiesViewController
    }
    
    // MARK : - CitiesViewRouter
    
    func presentCityWeather(for city: City) {
        self.city = city
        citiesViewController?.performSegue(withIdentifier: "CitiesSceneToCityWeatherSceneSegue", sender: nil)
    }
    
    func presentAddCity(addCityPresenterDelegate: GMSAutocompleteViewControllerDelegate) {
        self.addCityPresenterDelegate = addCityPresenterDelegate
        let autocompleteController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        autocompleteController.autocompleteFilter = filter
        autocompleteController.delegate = self.addCityPresenterDelegate
        citiesViewController?.present(autocompleteController, animated: true, completion: nil)
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let cityWeatherViewController = segue.destination as? CityWeatherViewController {
            cityWeatherViewController.configurator.configure(citiesViewController: cityWeatherViewController, city: city)
        }
    }  
}
