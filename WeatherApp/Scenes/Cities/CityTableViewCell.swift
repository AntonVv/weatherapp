//
//  CityTableViewCell.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell, CityCellView {

    @IBOutlet private weak var cityLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var infoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func display(name: String) {
        self.cityLabel?.text = name
    }
    
    func display(weather: Weather?) {
        if let weather = weather {
            self.temperatureLabel.text = weather.midTempCelsiusString
            self.iconImageView.image = UIImage(named: weather.iconName)
            self.infoLabel.text = weather.fullInfoDescription
        }
    }
}

extension CityTableViewCell {
    static func cellReuseIdentifier() -> String {
        return String(describing: self)
    }
}
