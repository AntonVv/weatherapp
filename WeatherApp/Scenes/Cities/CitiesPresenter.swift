//
//  CitiesPresenter.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation
import GooglePlaces

protocol CitiesView:class {
    func refreshCitiesView()
    func deleteAnimated(row: Int)
    func addAnimated(row: Int)
    func refreshCity(at row:Int)
    func endEditing()
}

protocol CityCellView {
    func display(name: String)
    func display(weather: Weather?)
}

protocol CitiesPresenter {
    var numberOfCities: Int { get }
    var router: CitiesViewRouter { get }
    
    func viewDidLoad()
    func configure(cell: CityCellView, forRow row: Int)
    func didSelect(row: Int)
    func canEdit(row: Int) -> Bool
    func deleteButtonPressed(row: Int)
    func addButtonPressed()
}

final class CitiesPresenterImplementation:NSObject, CitiesPresenter,GMSAutocompleteViewControllerDelegate {
    fileprivate weak var view: CitiesView?

    fileprivate let displayCitiesUseCase: DisplayCitiesListUseCase
    fileprivate let addCityUseCase: AddCityUseCase
    fileprivate let deleteCityUseCase: DeleteCityUseCase
    fileprivate let getWeatherUseCase: GetWeatherUseCase

    internal let router: CitiesViewRouter
    
    var cities = [City]()
    
    var numberOfCities: Int {
        return cities.count
    }
    
    
    init(view: CitiesView,
         displayCitiesUseCase: DisplayCitiesListUseCase,
         deleteCityUseCase: DeleteCityUseCase,
         addCityUseCase:AddCityUseCase,
         getWeatherUseCase: GetWeatherUseCase,
         router: CitiesViewRouter) {
        self.view = view
        self.displayCitiesUseCase = displayCitiesUseCase
        self.deleteCityUseCase = deleteCityUseCase
        self.addCityUseCase = addCityUseCase
        self.getWeatherUseCase = getWeatherUseCase
        self.router = router
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - CitiesPresenter
    
    func viewDidLoad() {

        self.displayCitiesUseCase.displayCities { (result) in
            switch result {
            case let .success(cities):
                self.handleCitiesReceived(cities)
            case let .failure(error):
                self.handleCitiesError(error)
            }
        }
        self.registerForUpdateCityNotification()
    }
    
    
    func configure(cell: CityCellView, forRow row: Int) {
        let city = cities[row]
        cell.display(name: city.name)
        cell.display(weather: city.currentWeather)
    }
    
    func didSelect(row: Int) {
        let city = cities[row]
        router.presentCityWeather(for: city)
    }
    
    func canEdit(row: Int) -> Bool {
        return true //if last city - then false
    }
    
    func deleteButtonPressed(row: Int) {
        view?.endEditing()
        
        let city = cities[row]
        deleteCityUseCase.delete(city: city) { (result) in
            switch result {
            case .success():
                self.handleCityDeleted(city: city)
            case let .failure(error):
                self.handleCityDeletedError(error: error)
            }
        }
    }
    
    func addButtonPressed() {
        router.presentAddCity(addCityPresenterDelegate: self)
    }
    
    // MARK: - GMSAutocompleteViewControllerDelegate
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        addCityUseCase.add(parameters: CityParameters(name:place.name,
                                                      lat: place.coordinate.latitude,
                                                      lng: place.coordinate.longitude,
                                                      placeId:place.placeID)) { (result) in
                                                        switch result {
                                                        case let .success(city):
                                                            self.handleCityAdded(city)
                                                        case let .failure(error):
                                                            self.handleAddCityError(error)
                                                        }
        }
        
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    
    // MARK: - Private
    
    fileprivate func getCurrentWeather(for city:City) {
        
        getWeatherUseCase.getCurrentWeather(in: city) { (result) in
            switch result {
            case let .success(resultCity):
                self.handleGetWheather(resultCity)
            case let .failure(error):
                self.handleGetWheatherError(error)
            }
        }
    }
    fileprivate func handleCitiesReceived(_ cities: [City]) {
        self.cities = cities
        self.cities.forEach({getCurrentWeather(for: $0)})
        view?.refreshCitiesView()
    }
    
    fileprivate func handleCitiesError(_ error: Error) {
        // Here we could check the error code and display a localized error message
    }
    
    fileprivate func handleCityAdded(_ city: City) {
        
        getCurrentWeather(for: city)
        cities.append(city)
        view?.addAnimated(row: cities.count - 1)
    }
    
    fileprivate func handleAddCityError(_ error: Error) {
        
    }
    
    
    fileprivate func handleCityDeleted(city: City) {
        if let row = cities.index(of: city) {
            cities.remove(at: row)
            view?.deleteAnimated(row: row)
        }
    }
    
    fileprivate func handleCityDeletedError(error: Error) {
        
    }
    
    fileprivate func handleGetWheather(_ city:City) {
        if let row = cities.index(of: city) {
            cities[row] = city
            view?.refreshCity(at: row)
        }
    }
    
    fileprivate func handleGetWheatherError(_ error:Error) {
        
    }
    
    
    
    fileprivate func registerForUpdateCityNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveUpdateCityNotification),
                                               name: Notification.Name("updateCity"),
                                               object: nil)
    }
    
    @objc fileprivate func didReceiveUpdateCityNotification(_ notification: Notification) {
        if let city = notification.object as? City {
            if let row = cities.index(of: city) {
                cities[row] = city
                view?.refreshCity(at: row)
            }
        }
    }
    
}
