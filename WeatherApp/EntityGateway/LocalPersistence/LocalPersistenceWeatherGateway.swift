//
//  LocalPersistenceWeatherGateway.swift
//  WeatherApp
//
//  Created by Mac on 29.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

protocol LocalPersistenceWeatherGateway {
    func addCurrentWeather(to city:City, weather: Weather, completionHandler: @escaping (_ city: Result<City>)->Void)
    func addForecastWeather(to city:City, forecast:[Weather], completionHandler: @escaping (_ city: Result<City>)->Void)
}

final class CoreDataWeatherGateway: LocalPersistenceWeatherGateway {
    
    let viewContext: NSManagedObjectContextProtocol
    
    init(viewContext: NSManagedObjectContextProtocol) {
        self.viewContext = viewContext
    }
    
    func addCurrentWeather(to city: City, weather: Weather, completionHandler: @escaping (Result<City>)->Void) {
        
        let predicate = NSPredicate(format: "placeId==%@", city.id)
        
        if let coreDataCities = try? viewContext.allEntities(withType: CoreDataCity.self, predicate: predicate),
            let coreDataCity = coreDataCities.first {
            
            guard let coreDataWeather = viewContext.addEntity(withType: CoreDataWeather.self) else {
                return
            }
            coreDataWeather.populate(with: weather)
            coreDataCity.currentWeather = coreDataWeather
            completionHandler(.success(coreDataCity.city))
        } else {
            completionHandler(.failure(CoreError(message: "Failed retrieving cities the data base")))
            return
        }
        
        do {
            try viewContext.save()
        } catch {
            completionHandler(.failure(CoreError(message: "Failed saving the context")))
        }
    }

    
    func addForecastWeather(to city: City, forecast: [Weather], completionHandler: @escaping (Result<City>)->Void) {
        
        let predicate = NSPredicate(format: "placeId==%@", city.id)
        
        if let coreDataCities = try? viewContext.allEntities(withType: CoreDataCity.self, predicate: predicate),
            let coreDataCity = coreDataCities.first {
            
            coreDataCity.forecastWeathers = nil
            for weather in forecast {
                guard let coreDataWeather = viewContext.addEntity(withType: CoreDataWeather.self) else {
                    return
                }
                coreDataWeather.populate(with: weather)
                coreDataCity.addToForecastWeathers(coreDataWeather)
            }

            completionHandler(.success(coreDataCity.city))
        } else {
            completionHandler(.failure(CoreError(message: "Failed retrieving cities the data base")))
            return
        }
        
        do {
            try viewContext.save()
        } catch {
            completionHandler(.failure(CoreError(message: "Failed saving the context")))
        }
    }
  
}
