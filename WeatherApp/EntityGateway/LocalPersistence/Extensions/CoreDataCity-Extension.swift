//
//  CoreDataCity-Extension.swift
//  WeatherApp
//
//  Created by Mac on 29.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation
import CoreData
import GooglePlaces

extension CoreDataCity {
    
    var city: City {
        
        var forecast = forecastWeathers?.allObjects.map{($0 as! CoreDataWeather).weather}
        forecast?.sort {
            Double($0.timestamp!) < Double($1.timestamp!)
        }
        let city = City(id:placeId!,
                        name:name ?? "",
                        countryCode:countryCode ?? "",
                        coordinates:CLLocationCoordinate2D(latitude: lat, longitude: lng),
                        currentWeather:currentWeather?.weather,
                        weatherForecast:forecast)
        return city
    }
    
    func populate(with place:GMSPlace) {
        name = place.name
        lat = place.coordinate.latitude
        lng = place.coordinate.longitude
        placeId = place.placeID
        zipCode = 1
        countryCode = ""
    }
    
    func populate(with parameters: CityParameters) {
        name = parameters.name
        lat = parameters.lat
        lng = parameters.lng
        placeId = parameters.placeId
        zipCode = 1
        countryCode = ""
    }
}


extension CoreDataWeather {
    var weather: Weather {
        return Weather(midTemp:midTemperature,maxTemp:maxTemperature,minTemp:minTemperature, weatherConditionId:conditionId, weatherConditionName: conditionName, icon:icon, timestamp: timestamp, rain: rainChance)
        
    }
    func populate(with weather:Weather) {
        midTemperature = Int16(weather.midTemp!)
        maxTemperature = Int16(weather.maxTemp!)
        minTemperature = Int16(weather.minTemp!)
        conditionId = Int16(weather.weatherConditionId!)
        conditionName = weather.weatherConditionName
        icon = weather.icon
        if let time = weather.timestamp {
            timestamp = time
        }
        if let rain = weather.rainChance {
            rainChance = rain
        }
    }
}
