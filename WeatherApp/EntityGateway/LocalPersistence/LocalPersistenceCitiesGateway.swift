//
//  LocalPersistenceCitiesGateway.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation



class CoreDataCitiesGateway: CitiesGateway {

    let viewContext: NSManagedObjectContextProtocol

    
    init(viewContext: NSManagedObjectContextProtocol) {
        self.viewContext = viewContext
    }
    
    // MARK: - CitiesGateway
    
    func fetchCities(completionHandler: @escaping FetchCitiesEntityGatewayCompletionHandler) {
        

        if let coreDataCities = try? viewContext.allEntities(withType: CoreDataCity.self) {
            let cities = coreDataCities.map { $0.city }
            completionHandler(.success(cities))
        } else {
            completionHandler(.failure(CoreError(message: "Failed retrieving cities the data base")))
        }
        
    }
    
    func add(parameters: CityParameters, completionHandler: @escaping AddCityEntityGatewayCompletionHandler) {
        
        let predicate = NSPredicate(format: "placeId=%@", parameters.placeId)

        if let coreDataCities = try? viewContext.allEntities(withType: CoreDataCity.self, predicate: predicate) {
            if coreDataCities.count > 0 {
                return
            }
        } else {
            completionHandler(.failure(CoreError(message: "Failed retrieving cities the data base")))
            return
        }

        guard let coreDataCity = viewContext.addEntity(withType: CoreDataCity.self) else {
            completionHandler(.failure(CoreError(message: "Failed adding the city in the data base")))
            return
        }
        
        coreDataCity.populate(with: parameters)
        
        do {
            try viewContext.save()
            completionHandler(.success(coreDataCity.city))
        } catch {
            viewContext.delete(coreDataCity)
            completionHandler(.failure(CoreError(message: "Failed saving the context")))
        }
        
    }
    func delete(city: City, completionHandler: @escaping DeleteCityEntityGatewayCompletionHandler) {
        
        let predicate = NSPredicate(format: "placeId=%@", city.id)
        
        if let coreDataCities = try? viewContext.allEntities(withType: CoreDataCity.self, predicate: predicate),
            let coreDataCity = coreDataCities.first {
            viewContext.delete(coreDataCity)
        } else {
            completionHandler(.failure(CoreError(message: "Failed retrieving cities the data base")))
            return
        }
        
        do {
            try viewContext.save()
            completionHandler(.success())
        } catch {
            completionHandler(.failure(CoreError(message: "Failed saving the context")))
        }
    }
}
