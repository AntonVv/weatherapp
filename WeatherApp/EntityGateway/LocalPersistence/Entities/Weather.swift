//
//  Weather.swift
//  WeatherApp
//
//  Created by Mac on 13.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation
protocol WeatherProtocol: InitializableWithData, InitializableWithJson {
    
}

extension WeatherProtocol {
    init(data: Data?) throws {
        guard let data = data,
            let jsonObject = try? JSONSerialization.jsonObject(with: data),
            let json = jsonObject as? [String: Any] else {
                throw NSError.createParseError()
        }
        
        try self.init(json: json)
    }
}

class Weather: WeatherProtocol {
    
    var midTemp: Int?
    var maxTemp: Int?
    var minTemp: Int?
    var dayTemp: Int?
    var nightTemp: Int?
    var timestamp: TimeInterval?
    var rainChance: Int16?
    var weatherConditionId: Int?
    var weatherConditionName: String?
    var icon: String?
    
    
    init(midTemp:Int16,maxTemp:Int16,minTemp:Int16, weatherConditionId:Int16, weatherConditionName: String?, icon:String?, timestamp: TimeInterval?, rain: Int16) {
        self.midTemp = Int(midTemp)
        self.maxTemp = Int(maxTemp)
        self.minTemp = Int(minTemp)
        self.weatherConditionId = Int(weatherConditionId)
        self.weatherConditionName = weatherConditionName
        self.icon = icon
        self.timestamp = timestamp
        self.rainChance = rain
    }
    
    required init(json: [String : Any]) throws {
        guard let mainDic = json["main"] as? Dictionary<String,Any>,
            let minTemp = mainDic["temp_min"] as? Int,
            let maxTemp = mainDic["temp_max"] as? Int,
            let midTemp = mainDic["temp"] as? Int,
            let weathersArray = json["weather"] as? [Dictionary<String,Any>],
            let weatherConditionId = weathersArray.first?["id"] as? Int,
            let weatherConditionName = weathersArray.first?["description"] as? String,
            let icon = weathersArray.first?["icon"] as? String else
        {
            throw NSError.createParseError()
        }
        
        self.midTemp = midTemp
        self.maxTemp = maxTemp
        self.minTemp = minTemp
        self.weatherConditionId = weatherConditionId
        self.weatherConditionName = weatherConditionName
        self.icon = icon
    }
    
}

extension Weather {
    
    var midTempCelsiusString: String {
        guard let temp = midTemp else {
            return "--"
        }
        return "\(temp)°"
    }
    
    var minTempCelsiusString: String {
        guard let temp = minTemp else {
            return "--"
        }
        return "\(temp)°"
    }
    
    var maxTempCelsiusString: String {
        guard let temp = maxTemp else {
            return "--"
        }
        return "\(temp)°"
    }
    
    
    var dayTempCelsiusString: String {
        guard let temp = dayTemp else {
            return maxTempCelsiusString
        }
        return "\(temp)°"
    }
    
    var nightTempCelsiusString: String {
        guard let temp = nightTemp else {
            return minTempCelsiusString
        }
        return "\(temp)°"
    }
    
    var iconName: String {
        guard let icon = icon else {
            return ""
        }
        
        // bug in API - sometimes return wrong icon name
        if icon.characters.count > 3 {
            let index = icon.index(icon.startIndex, offsetBy: 3)
            return icon.substring(to: index)
        }
        return icon
    }
    
    var dateString: String {
        
        guard let time = timestamp else {
            return "--"
        }
        
        let date = Date(timeIntervalSince1970: time)
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM EEE"
        
        return formatter.string(from: date)
    }
    
    var rainInfo: String {
        
        guard let rainChance = rainChance,
            rainChance > 0 else {
                return ""
        }
        return ", вероятность \(rainChance)%"
    }
    
    
    var fullInfoDescription: String {
        
        if let condition = weatherConditionName {
            return condition + rainInfo
        }
        return rainInfo
        
    }
}
