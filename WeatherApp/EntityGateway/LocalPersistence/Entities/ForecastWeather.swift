//
//  WeatherForecast.swift
//  WeatherApp
//
//  Created by Mac on 29.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation


final class ForecastWeather: WeatherProtocol {
    
    var forecast = [Weather]()
    
    required init(json: [String : Any]) throws {
        
        guard let listArray = json["list"] as? [Dictionary<String,Any>] else {
            throw NSError.createParseError()
        }
        
        for dict in listArray {
            
            guard let tempDict = dict["temp"] as? Dictionary<String,Any>,
                let minTemp = tempDict["min"] as? Int,
                let maxTemp = tempDict["max"] as? Int,
                let midTemp = tempDict["eve"] as? Int,
                let dayTemp = tempDict["day"] as? Int,
                let nightTemp = tempDict["night"] as? Int,
                let timeStamp = dict["dt"] as? Double,
                let weathersArray = dict["weather"] as? [Dictionary<String,Any>],
                let weatherConditionId = weathersArray.first?["id"] as? Int,
                let weatherConditionName = weathersArray.first?["description"] as? String,
                let icon = weathersArray.first?["icon"] as? String else
            {
                throw NSError.createParseError()
            }
            let weather = Weather(midTemp: Int16(midTemp), maxTemp: Int16(maxTemp), minTemp: Int16(minTemp), weatherConditionId: Int16(weatherConditionId), weatherConditionName: weatherConditionName, icon: icon, timestamp:timeStamp, rain:0)
            weather.dayTemp = dayTemp
            weather.nightTemp = nightTemp
            forecast.append(weather)

            guard let rainChance = dict["rain"] as? Int16 else {
                //optional values
                continue
            }
            weather.rainChance = rainChance
        }
    }
}

