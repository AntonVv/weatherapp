//
//  City.swift
//  WeatherApp
//
//  Created by Mac on 29.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation
import CoreLocation

///temp

struct City: InitializableWithData, InitializableWithJson {
    let id: String
    let name: String
    let countryCode: String
    let coordinates: CLLocationCoordinate2D
    var currentWeather: Weather?
    var weatherForecast: [Weather]?
    var didUpdateForecastonThisLaunch = false
    
    //let actualWeather: Weather
    init() {
        self.id = ""
        self.name = ""
        self.countryCode = "CountryCode"
        self.coordinates = CLLocationCoordinate2DMake(0, 0)
    }
    init(data: Data?) throws {
        // Here you can parse the JSON or XML using the build in APIs or your favorite libraries
        guard let data = data,
            let jsonObject = try? JSONSerialization.jsonObject(with: data),
            let json = jsonObject as? [String: Any] else {
                throw NSError.createParseError()
        }
        
        try self.init(json: json)
        
    }
    
    init(json: [String : Any]) throws {
        guard let id = json["Id"] as? String,
            let name = json["name"] as? String
            //            let title = json["Title"] as? String,
            //            let author = json["Author"] as? String,
            //            let pages = json["Pages"] as? Int,
            //            let releaseDate = json["ReleaseDate"] as? Date
            else {
                throw NSError.createParseError()
        }
        
        self.id = id
        self.name = name
        self.countryCode = "CountryName"
        self.coordinates = CLLocationCoordinate2DMake(0, 0)
        //self.actualWeather = Weather()
    }
    
    init(id: String, name: String, countryCode: String, coordinates:CLLocationCoordinate2D, currentWeather: Weather?,weatherForecast: [Weather]?) {
        self.id = id
        self.name = name
        self.countryCode = countryCode
        self.coordinates = coordinates
        self.currentWeather = currentWeather
        self.weatherForecast = weatherForecast
    }
}
extension City: Equatable { }

func == (lhs: City, rhs: City) -> Bool {
    return lhs.id == rhs.id
}
