//
//  GetWeatherForCity.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

enum WeatherPeriod: Int {
    case forecast5Days
    case forecast10Days
    case forecast16Days
    case current

    public var urlPart: String {
        
        switch self {
        case .current:
            return EndPoints.currentWeatherURLPart
        case .forecast5Days:
            return EndPoints.forecastWeatherURLPart + "cnt=5&"
        case .forecast10Days:
            return EndPoints.forecastWeatherURLPart + "cnt=10&"
        case .forecast16Days:
            return EndPoints.forecastWeatherURLPart + "cnt=16&"
        }
    }
    
    public var weathersCount: Int {
        switch self {
        case .current:
            return 1
        case .forecast5Days:
            return 5
        case .forecast10Days:
            return 10
        case .forecast16Days:
            return 16
        }
    }
}

struct GetWeatherForCityApiRequest: ApiRequest {

    let getWeatherForCity: City
    let weatherPeriod: WeatherPeriod
    
    var urlRequest: URLRequest {
        let url: URL! = URL(string: EndPoints.baseURL + weatherPeriod.urlPart + getWeatherForCity.urlStringParametrs + "&units=metric&lang=ru&APPID=\(ApiRequestConstants.apiKey)")
        var request = URLRequest(url: url)
        
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request
    }
}

extension City {
    public var urlStringParametrs: String {
        
        return "lat=\(coordinates.latitude)&lon=\(coordinates.longitude)"
    }
}
