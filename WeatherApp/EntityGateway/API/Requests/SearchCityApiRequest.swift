//
//  SearchCity.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

struct SearchCityApiRequest: ApiRequest {

    let name: String
    
    init(name: String) {
        self.name = name
    }
    var urlRequest: URLRequest {
        
        let url: URL! = URL(string: EndPoints.baseURL +  "&APPID=\(ApiRequestConstants.apiKey)")
        var request = URLRequest(url: url)
        
        
        request.httpMethod = "GET"
        
        return request
    }
}
