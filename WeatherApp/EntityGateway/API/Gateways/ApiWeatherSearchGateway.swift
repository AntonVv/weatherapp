//
//  ApiWeatherSearchGateway.swift
//  WeatherApp
//
//  Created by Mac on 29.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation


protocol ApiWeatherSearchGateway {
    
    func getCurrentWeather(in city:City, completionHandler:@escaping (_ weather: Result<Weather>) -> Void)
    
    func getCurrentWeather(in cities:[City], completionHandler:@escaping (_ weather: Result<[Weather]>) -> Void)
    
    func getWeather(in city:City, period:WeatherPeriod, completionHandler:@escaping (_ weather: Result<[Weather]>) -> Void)
    
    //func getWeather(in city:City, period:WeatherPeriod, completionHandler: (_ weather: Result<[Weather]>) -> Void)
}

final class ApiWeatherSearchGatewayImplementation: ApiWeatherSearchGateway {
    
    let apiWeatherService: ApiWeatherService
    
    init(apiWeatherService: ApiWeatherService) {
        self.apiWeatherService = apiWeatherService
    }
    
    // MARK: - GetWeatherUseCase
    
    func getCurrentWeather(in city: City, completionHandler: @escaping (Result<Weather>) -> Void) {
        
        let getWeatherRequest = GetWeatherForCityApiRequest(getWeatherForCity: city, weatherPeriod: WeatherPeriod.current)
        apiWeatherService.execute(request: getWeatherRequest) { (result: Result<ApiResponse<Weather>>) in
            
            switch result {
            case let .success(response):
                let weather = response.entity
                completionHandler(.success(weather))
            case let .failure(error):
                completionHandler(.failure(error))
            }
        }
    }
    
    func getCurrentWeather(in cities: [City], completionHandler: @escaping (Result<[Weather]>) -> Void) {
        
        
    }
    
    func getWeather(in city: City, period: WeatherPeriod, completionHandler:@escaping (Result<[Weather]>) -> Void) {
        let getWeatherRequest = GetWeatherForCityApiRequest(getWeatherForCity: city, weatherPeriod: period)
        apiWeatherService.execute(request: getWeatherRequest) { (result: Result<ApiResponse<ForecastWeather>>) in
            
            switch result {
            case let .success(response):
                let weatherForecast = response.entity
                completionHandler(.success(weatherForecast.forecast))
            case let .failure(error):
                completionHandler(.failure(error))
            }
        }
    }
}
