//
//  CitiesGateway.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

typealias FetchCitiesEntityGatewayCompletionHandler = (_ cities: Result<[City]>) -> Void
typealias AddCityEntityGatewayCompletionHandler = (_ cities: Result<City>) -> Void
typealias DeleteCityEntityGatewayCompletionHandler = (_ cities: Result<Void>) -> Void

protocol CitiesGateway {
    func fetchCities(completionHandler: @escaping FetchCitiesEntityGatewayCompletionHandler)
    func add(parameters: CityParameters, completionHandler: @escaping AddCityEntityGatewayCompletionHandler)
    func delete(city: City, completionHandler: @escaping DeleteCityEntityGatewayCompletionHandler)
}
