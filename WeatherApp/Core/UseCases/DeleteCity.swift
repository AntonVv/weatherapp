//
//  DeleteCity.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

typealias DeleteCityUseCaseCompletionHandler = (_ cities: Result<Void>)->Void

protocol DeleteCityUseCase {
    func delete(city: City, completionHandler: @escaping DeleteCityUseCaseCompletionHandler)
}


final class DeleteCityUseCaseImplementation:DeleteCityUseCase {
    let citiesGateway: CitiesGateway
    
    init(citiesGateway: CitiesGateway) {
        self.citiesGateway = citiesGateway
    }
    
    // MARK: - DeleteCityUseCase

    func delete(city: City, completionHandler: @escaping DeleteCityUseCaseCompletionHandler) {
        self.citiesGateway.delete(city: city) { (result) in
            completionHandler(result)
        }
    }
}


