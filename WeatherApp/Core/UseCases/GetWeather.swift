//
//  GetWeather.swift
//  WeatherApp
//
//  Created by Mac on 29.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

protocol GetWeatherUseCase {
    func getCurrentWeather(in city:City, completionHandler:@escaping (_ weather: Result<City>) -> Void)
    
    func getCurrentWeather(in cities:[City], completionHandler:@escaping (_ weather: Result<[City]>) -> Void)
    
    func getWeather(in city:City, period:WeatherPeriod, completionHandler:@escaping (_ weather: Result<City>) -> Void)
}

final class GetWeatherUseCaseImplementation: GetWeatherUseCase {
    
    let apiWeatherSearchGateway: ApiWeatherSearchGateway
    let coreDataWeatherGateway: CoreDataWeatherGateway

    init(apiWeatherSearchGateway: ApiWeatherSearchGateway,
         coreDataWeatherGateway:
        CoreDataWeatherGateway) {
        self.apiWeatherSearchGateway = apiWeatherSearchGateway
        self.coreDataWeatherGateway = coreDataWeatherGateway
    }
    
    // MARK: - GetWeatherUseCase
    
    func getCurrentWeather(in city: City, completionHandler: @escaping (Result<City>) -> Void) {
        
        self.apiWeatherSearchGateway.getCurrentWeather(in: city) { (apiResult) in
            switch apiResult {
            case let .success(weather):
                self.handleSuccessCurrentWeather(in: city, weather: weather, completionHandler: { (results) in
                    completionHandler(results)
                })
            case let .failure(error):
                completionHandler(.failure(error))
                break
            }
        }
    }
    
    
    func getCurrentWeather(in cities: [City], completionHandler: @escaping (Result<[City]>) -> Void) {
        
    }
    
    func getWeather(in city: City, period: WeatherPeriod, completionHandler:@escaping (Result<City>) -> Void) {
        self.apiWeatherSearchGateway.getWeather(in: city, period: period) { (apiResult) in
            switch apiResult {
            case let .success(forecast):
                self.handleSuccessForecast(in: city, forecast: forecast, completionHandler: { (results) in
                    completionHandler(results)
                })
            case let .failure(error):
                completionHandler(.failure(error))
            break
            }
        }
    }

    // MARK: - private
    
    private func handleSuccessForecast(in city:City, forecast: [Weather], completionHandler: @escaping (_ result: Result<City>)->Void) {
        
        self.coreDataWeatherGateway.addForecastWeather(to: city, forecast: forecast, completionHandler: { (localResult) in
            
            completionHandler(localResult)
        })
    }
    
    private func handleSuccessCurrentWeather(in city:City, weather: Weather, completionHandler: @escaping (_ result: Result<City>)->Void) {
        
        self.coreDataWeatherGateway.addCurrentWeather(to: city, weather: weather, completionHandler: { (localResult) in
            
            completionHandler(localResult)
        })
    }
}
