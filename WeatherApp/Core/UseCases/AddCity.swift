//
//  AddCity.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

typealias AddCityUseCaseCompletionHandler = (_ cities: Result<City>) -> Void

protocol AddCityUseCase {
    func add(parameters: CityParameters, completionHandler: @escaping AddCityUseCaseCompletionHandler)
}

struct CityParameters {
    var name: String
    var lat: Double
    var lng: Double
    var placeId: String
    
    init(name: String, lat: Double, lng: Double, placeId: String) {
        
        self.name = name
        self.lat = lat
        self.lng = lng
        self.placeId = placeId
    }
}

final class AddCityUseCaseImplementation: AddCityUseCase {
    let citiesGateway: CitiesGateway
    
    init(citiesGateway: CitiesGateway) {
        self.citiesGateway = citiesGateway
    }
    
    // MARK: - AddCityUseCase
    
    func add(parameters: CityParameters, completionHandler: @escaping AddCityUseCaseCompletionHandler) {
        self.citiesGateway.add(parameters: parameters, completionHandler: completionHandler)
    }
}
