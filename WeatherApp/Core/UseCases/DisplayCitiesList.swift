//
//  DisplayCitiesList.swift
//  WeatherApp
//
//  Created by Mac on 27.08.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation


typealias DisplayCitiesUseCaseCompletionHandler = (_ cities: Result<[City]>) -> Void


protocol DisplayCitiesListUseCase {
    func displayCities(completionHandler: @escaping DisplayCitiesUseCaseCompletionHandler)
}
final class DisplayCitiesListUseCaseImplementation: DisplayCitiesListUseCase {

    let citiesGateway:CitiesGateway
    
    init(citiesGateway: CitiesGateway) {
        self.citiesGateway = citiesGateway
    }
    
    // MARK: - DisplayCitiesUseCase

    func displayCities(completionHandler: @escaping DisplayCitiesUseCaseCompletionHandler) {
        self.citiesGateway.fetchCities {(result) in
            completionHandler(result)
        }
    }
}
